import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView
} from 'react-native';
import './config/ReactotronConfig';
import Post from './components/Post';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EE7777',

  },

  header: {
    backgroundColor: '#FFFFFF',
  },

});

export default class App extends Component {
  render() {
    return (
      <View style = {styles.header}/>
      <ScrollView style = {styles.container}>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
      </ScrollView>
    );
  }
}
