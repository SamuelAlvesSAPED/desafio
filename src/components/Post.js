import React, {Component} from 'react';
import{
  View,
  Text,
  StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({

  box: {
    backgroundColor: '#FFFFFF',
    marginVertical: 20,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default class Post extends Component{
  render(){
    return(
        <View style = {styles.box}/>
    );
  }
}


